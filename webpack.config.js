var path = require('path');
var webpack = require('webpack');

const cssnext = require('postcss-cssnext');

// const entries = [
//     'webpack-dev-server/client?http://localhost:3000',
//     'webpack/hot/only-dev-server',
//     'react-hot-loader/patch',
//     './client/src/main.js'
// ];

var port = process.env.PORT || 3000;

const entries = [
  'webpack-dev-server/client?http://localhost:' + port,
  'webpack/hot/only-dev-server',
  'react-hot-loader/patch',
  './client/src/main.tsx'
];


module.exports = {
  /* redbox-react/README.md */
  devtool: 'eval',
  entry: entries,
  output: {
    path: path.join(__dirname, 'client/public/dist'),
    filename: 'bundle.js',
    publicPath: '/dist'
    /* redbox-react/README.md */
    // ,devtoolModuleFilenameTemplate: '/[absolute-resource-path]'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ],
  resolve: {
    extensions: ['', '.ts', '.tsx', '.js']
  },
  resolveLoader: {
    'fallback': path.join(__dirname, 'node_modules')
  },
  module: {
     preLoaders: [
      {
        test: /\.tsx?$/,
        loader: 'tslint',
        include: path.join(__dirname, 'client/src')
      }
    ],
    loaders: [
      {
        test: /\.jsx?/,
        loaders: ['babel'],
        include: path.join(__dirname, 'client/src')
      },
      {
        test: /\.tsx?$/,
        loaders: [
                'babel',                  
                'ts-loader'
            ],
        include: path.join(__dirname, 'client/src')
      },
      {
        test: /\.css$/,
        loader: 'style!css!postcss'
      }
    ]
  },
  postcss: function () {
    return [cssnext];
  },
  tslint: {
    emitErrors: true,
    failOnHint: true
  }
};
