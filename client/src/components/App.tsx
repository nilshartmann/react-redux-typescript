import * as React from 'react';

import { Row, Column, ColumnRow } from './ui/FoundationComponents';
import MessageView from './ui/MessageView';
const connect = require('react-redux').connect;
import { IContact, IGlobalState, IMessagesState } from '../types';

function mapStateToProps(state: IGlobalState): IAppProps {
  return {
    currentUsername: state.auth.username,
    messages: state.messages
  };
}

type IAppProps = {
  children?: any,
  currentUsername: string,
  messages: IMessagesState
}

function App({children, currentUsername, messages}: IAppProps) {
  return <main>
    <header>
      <div className='title'><h1>Contacts</h1></div>
      <div className='banner'>
        <button type='button' className='secondary small button'>{currentUsername ? `Logged in as ${currentUsername}` : 'Please login'}</button>
      </div>
    </header>
    {children}
    <MessageView messages={messages} />
  </main>;
}

export default connect(
  mapStateToProps
)(App);



