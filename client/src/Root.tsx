import * as React from 'react';

// redux
const { Provider } = require('react-redux');

// Router
const { Router } = require('react-router');


import configureRoutes from './configureRoutes';

// https://github.com/gaearon/react-hot-boilerplate/pull/61#issuecomment-218333616
// https://github.com/rybon/counter-hmr/blob/e651ce25b3a307f13ca53c977f9e8709ba873407/src/components/Root.jsx
const Root = ({store, history}) => (
  <Provider store={store}>
    <Router history={history}>
      {configureRoutes(store)}
    </Router>
  </Provider>
);

export default Root;
