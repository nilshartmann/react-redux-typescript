#!/bin/sh
FONTS_DIR=./public/fonts
mkdir -p $FONTS_DIR

#X="src: local('Gudea'), url(https://fonts.gstatic.com/s/gudea/v4/8dc6B2s_9y2FVH9c_TZ6mIDGDUGfDkXyfkzVDelzfFk.woff2) format('woff2');"

#echo $X |sed 's/.*url(\([A-Za-z0-9_\/\.:]*\)).*/\1/'

#exit;

curl https://fonts.googleapis.com/css?family=Gudea:400,400italic,700 >$FONTS_DIR/google-fonts.css

FONTURL=https:\\/\\/fonts.gstatic.com

cat $FONTS_DIR/google-fonts.css | grep url | while read i
do
  URL=$(echo $i| sed 's/.*url(\(.*\)) .*/\1/')

  # Host abschneiden
  TARGET=`echo $URL|sed 's/'${FONTURL}'//'`

  TARGET_FILE=$FONTS_DIR$TARGET

  TARGET_DIR=`dirname $TARGET_FILE`

  mkdir -p $TARGET_DIR

  

  curl $URL -o $TARGET_FILE
done;

sed 's/'${FONTURL}\\/'//' $FONTS_DIR/google-fonts.css >$FONTS_DIR/google-fonts-offline.css
rm $FONTS_DIR/google-fonts.css
