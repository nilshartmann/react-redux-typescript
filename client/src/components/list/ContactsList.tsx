import * as React from 'react';
import { Link } from 'react-router';
const connect = require('react-redux').connect;

import * as classnames from 'classnames';

import FilterField from './FilterField';

import { IGlobalState, IContact, ISelectedContactState } from '../../types';
import { setFilter, clearFilter } from '../../actions';

interface IContactsListStateProps {
  contacts: IContact[];
  filter: string;
  selectedContact: ISelectedContactState;
}

interface IContactsListDispatchProps {
  setFilter: (filter: string) => void;
  clearFilter: () => void;
}

function mapStateToProps(state: IGlobalState, ownProps): IContactsListStateProps {
  return {
    contacts: state.contacts,
    filter: state.filter,
    selectedContact: state.selectedContact
  };
}

function mapDispatchToProps(dispatch): IContactsListDispatchProps {
  return {
    setFilter: (filter) => dispatch(setFilter(filter)),
    clearFilter: () => dispatch(clearFilter)
  };
}

function ContactsList({selectedContact, contacts, filter, setFilter}: IContactsListDispatchProps & IContactsListStateProps) {
  const ulClasses = classnames('menu vertical', {
    'disabled': selectedContact.editing
  });

  const lowercaseFilter = filter ? filter.toLocaleLowerCase() : null;
  const visibleContacts = filter ? contacts.filter(contact => contact.pk === selectedContact.pk || contact.name.toLocaleLowerCase().indexOf(lowercaseFilter) !== -1) : contacts;

  const menuStyles = {
    marginBottom: '1rem'
  };

  return <div>
    <FilterField filter={filter} onFilterChangeHandler={setFilter} />

    <ul className={ulClasses} style={menuStyles}>
      {visibleContacts.map(contact => {
        const liClasses = classnames({
          'active': contact.pk === selectedContact.pk,
        });

        const c = selectedContact.editing ? e => e.preventDefault() : undefined;

        return <li key={contact.pk}
        onClick={c}
          className={ liClasses }>
          {selectedContact.editing ?
             <a>{contact.name}</a>
          :
            <Link to={`/contacts/${contact.pk}`}>{contact.name}</Link>
          }
        </li>
          ;
      }) }
    </ul>
    {selectedContact.editing ? <a className='secondary button tiny float-right'>Add...</a> : <Link to='/contacts/new' className='success button tiny float-right'>Add...</Link>}
  </div>;
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactsList);

