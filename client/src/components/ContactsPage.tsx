import * as React from 'react';

const connect = require('react-redux').connect;
import { bindActionCreators } from 'redux';

import { Layout } from './ui/FoundationComponents';

import ContactsList from './list/ContactsList';
import ContactEditor from './editor/ContactEditor';

import { IContact, IGlobalState, ISelectedContactState } from '../types';

import { fetchContacts } from '../actions';

interface IContactsPageStateProps {
  contacts: IContact[];
  selectedContact: ISelectedContactState;
};

interface IContactsPageDispatchProps {
  fetchContacts: () => void;
}

function mapStateToProps(state: IGlobalState): IContactsPageStateProps {
  return {
    contacts: state.contacts,
    selectedContact: state.selectedContact,
  };
}


function mapDispatchToProps(dispatch): IContactsPageDispatchProps {
  return {
    fetchContacts: () => dispatch(fetchContacts())
  };
}

class ContactsPage extends React.Component<IContactsPageStateProps & IContactsPageDispatchProps, any> {
  componentWillMount() {
    this.props.fetchContacts();
  }
  render() {
    const { contacts, selectedContact } = this.props;
    const currentContact = contacts.find(contact => contact.pk === selectedContact.pk);

    return <div className='ContactsPage'>
      <div className='Left'><ContactsList/></div>
      <div className='Right'><ContactEditor contact={currentContact}/></div>
    </div>;

    // return <TwoColumnRow left={contactsList} right={editor} />
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContactsPage);


