# Run the frontend

* Backend must be running
* `npm install`
* `npm start`
* Open browser at `http://localhost:3030`
*  Sign in either as `klaus`/`geheim` or `maja`/`secret`


# TODO


- Example for state: Form Component
- Refactoring of Component names


Modus:

READ:
 * Filter ist eingeschaltet (enabled)
 * Liste ist enabled
 * Formular Read-Only
 * Edit Button (enabled) im Formular

Nach Klick auf Edit:
* Filter und Liste disabled
* Formular bearbeitbar
* Edit Button => Save, links Cancel (kleiner)

Save Button:
* Daten speichern
* wieder zu READ

Cancel:
* Verwerfen => READ


