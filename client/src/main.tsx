// React and Hot Loader
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';

// Router <-> Redux
const { syncHistoryWithStore } = require('react-router-redux');
import { browserHistory } from 'react-router';

// styles
import './styles/styles.css';

// The Application
import Root from './Root';
import configureStore from './configureStore';

// Redux Store 
const store = configureStore(browserHistory);

// Connect History and Redux
const history = syncHistoryWithStore(browserHistory, store);

// Render Application
const mountPoint = document.getElementById('mount');
ReactDOM.render(
  <AppContainer><Root store={store}  history={history} /></AppContainer>,
  mountPoint
);

declare var module: any;
if (module.hot) {
  module.hot.accept('./Root', () => {
    const NextApp = require('./Root').default;
    ReactDOM.render(
      <AppContainer>
        <NextApp store={store}  history={history} />
      </AppContainer>,
      mountPoint
    );
  });
}
